// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBQnrM_NPIiktdCsTy_H_SovFOfNrDvFQ4",
    authDomain: "my-wallet-160fc.firebaseapp.com",
    projectId: "my-wallet-160fc",
    storageBucket: "my-wallet-160fc.appspot.com",
    messagingSenderId: "267871654101",
    appId: "1:267871654101:web:8ac7189a112eaa86d3125c",
    measurementId: "G-9DLX43EQ01"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
