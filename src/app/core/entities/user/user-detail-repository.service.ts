import {Injectable} from "@angular/core";
import {Subscription} from "rxjs";
import {AngularFirestore} from "@angular/fire/firestore";
import {UserDetail} from "./user-detail";
import {map, take} from "rxjs/operators";
import {Store} from "@ngrx/store";

import * as fromAuth from "../../authentication/auth.reducer";
import * as AuthAction from "../../authentication/auth.actions";

@Injectable({
  providedIn: 'root'
})
export class UserDetailRepositoryService {
  private subs: Array<Subscription> = [];

  constructor(
    private $db: AngularFirestore,
    private $store: Store<fromAuth.State>
  ) {
  }

  public getUserByAuthUserUID(authUserUID: string) {
    this.subs.push(this.$db.collection('userDetail', ref => ref.where('authUserUID', '==', authUserUID))
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray.map(doc => {
            return {
              id: doc.payload.doc.id,
              // @ts-ignore
              ...doc.payload.doc.data()
            };
          });
        }),
      ).subscribe((result) => {
        const userDetail: UserDetail = result[0];

        this.$store.dispatch(new AuthAction.LoadUserDetail(userDetail));
      }));
  }

  public addUserDetail(userDetail: UserDetail): void {
    this.$db.collection('userDetail').add(userDetail);
  }

  public cancelSubscriptions(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
