export interface UserDetail {
  authUserUID?: string;
  firstName: string;
  lastName: string;
  email: string;
  dateOfBirth: Date;
}
