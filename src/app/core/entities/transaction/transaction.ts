export interface Transaction {
  name: string;
  amount: number;
  date?: Date;
  expenseTypeId?: string
}
