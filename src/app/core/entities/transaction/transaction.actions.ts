import {Action} from "@ngrx/store";
import {Transaction} from "./transaction";

export const SET_TRANSACTIONS = '[Transaction] Set Transactions';
export const ADD_TRANSACTION = '[Transaction] Add Transaction';

export class SetTransactions implements Action {
  readonly type = SET_TRANSACTIONS;

  constructor(public payload: Transaction[]) {}
}

export class AddTransaction implements Action {
  readonly type = ADD_TRANSACTION;

  constructor(public payload: Transaction) {
  }
}

export type TransactionActions = SetTransactions | AddTransaction;
