import {Transaction} from "./transaction";
import {ADD_TRANSACTION, SET_TRANSACTIONS, TransactionActions} from "./transaction.actions";

import * as fromRoot from '../../../app.reducer';
import {createFeatureSelector, createSelector} from "@ngrx/store";

export interface TransactionState {
  lastAddedTransaction: Transaction;
  transactions: Transaction[];
}

export interface State extends fromRoot.State {
  transaction: TransactionState
}

const initialState: TransactionState = {
  lastAddedTransaction: null,
  transactions: [],
};

export function transactionReducer(state = initialState, action: TransactionActions) {
  switch (action.type) {
    case SET_TRANSACTIONS:
      return {
        ...state,
        transactions: action.payload
      };
    case ADD_TRANSACTION:
      return {
        lastAddedTransaction: action.payload,
        transactions: [...state.transactions, action.payload]
      };
    default:
      return state;
  }
}

export const getTransactionState = createFeatureSelector<TransactionState>('transaction');

export const getTransactions = createSelector(getTransactionState, (state: TransactionState) => state.transactions);
export const getLastAddedTransaction = createSelector(getTransactionState, (state: TransactionState) => state.transactions[length - 1]);
