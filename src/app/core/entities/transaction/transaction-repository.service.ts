import {Injectable} from '@angular/core';
import {map} from "rxjs/operators";
import {Transaction} from "./transaction";
import {AngularFirestore} from "@angular/fire/firestore";
import {Subject, Subscription} from "rxjs";
import {Store} from "@ngrx/store";

import * as fromTransaction from './transaction.reducer';
import * as TransactionAction from './transaction.actions';

@Injectable({
  providedIn: 'root'
})
export class TransactionRepositoryService {
  public subs: Array<Subscription> = [];

  constructor(
    private $db: AngularFirestore,
    private $store: Store<fromTransaction.State>
  ) {
  }

  public getAllTransactions(): void {
    this.subs.push(
      this.$db.collection('transactions')
        .snapshotChanges()
        .pipe(
          map(docArray => {
            return docArray.map(doc => {
              return {
                id: doc.payload.doc.id,
                // @ts-ignore
                ...doc.payload.doc.data()
              };
            });
          })
        )
        .subscribe((transactions: Array<Transaction>) => {
          this.$store.dispatch(new TransactionAction.SetTransactions(transactions));
        }));
  }

  public addTransaction(transaction: Transaction): void {
    this.$db.collection('transactions').add(transaction);
    this.$store.dispatch(new TransactionAction.AddTransaction(transaction));
  }

  public cancelSubscriptions(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
