export interface ExpenseType {
  name: string;
  color: string;
}
