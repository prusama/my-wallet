import {Injectable} from "@angular/core";
import {ExpenseType} from "./expense-type";
import {AngularFirestore} from "@angular/fire/firestore";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";

const EXPENSE_TYPES_COLLECTION_NAME = 'expenseType';

@Injectable({
  providedIn: 'root'
})
export class ExpenseTypeRepositoryService {

  constructor(
    private $db: AngularFirestore
  ) {
  }

  public insertExpenseType(expenseType: ExpenseType): Promise<any> {
    return this.$db.collection(EXPENSE_TYPES_COLLECTION_NAME).add(expenseType);
  }

  public getExpenseTypes(): Observable<Array<ExpenseType>> {
    return this.$db.collection(EXPENSE_TYPES_COLLECTION_NAME)
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray.map(doc => {
            return {
              id: doc.payload.doc.id,
              // @ts-ignore
              ...doc.payload.doc.data()
            };
          });
        })
      )
  }
}
