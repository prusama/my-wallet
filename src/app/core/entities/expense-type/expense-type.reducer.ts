import {ExpenseType} from "./expense-type";

import * as fromRoot from '../../../app.reducer';
import {ADD_EXPENSE_TYPE, ExpenseTypeActions, SET_EXPENSE_TYPES} from "./expense-type.actions";
import {createFeatureSelector, createSelector} from "@ngrx/store";

export interface ExpenseTypeState {
  expenseTypes: Array<ExpenseType>,
  lastAddedExpenseType: ExpenseType
}

export interface State extends fromRoot.State {
  expenseTypes: ExpenseTypeState
}

const initialState: ExpenseTypeState = {
  expenseTypes: [],
  lastAddedExpenseType: null
}

export function expenseTypeReducer(state: ExpenseTypeState = initialState, action: ExpenseTypeActions): ExpenseTypeState {
  switch (action.type) {
    case SET_EXPENSE_TYPES:
      return {
        ...state,
        expenseTypes: action.payload,
      };
    case ADD_EXPENSE_TYPE:
      return {
        lastAddedExpenseType: action.payload,
        expenseTypes: [...state.expenseTypes, action.payload]
      };
    default:
      return state;
  }
}

export const getExpenseTypeState = createFeatureSelector<ExpenseTypeState>('expenseType');

export const getExpenseTypes = createSelector(getExpenseTypeState, (state: ExpenseTypeState) => state.expenseTypes);
export const getLastAddedExpenseType = createSelector(getExpenseTypeState, (state: ExpenseTypeState) => state.lastAddedExpenseType);
