import {Action} from "@ngrx/store";
import {ExpenseType} from "./expense-type";

export const SET_EXPENSE_TYPES = '[ExpenseType] Set Expense Types';
export const ADD_EXPENSE_TYPE = '[ExpenseType] Add Expense Type';

export class SetExpenseTypes implements Action {
  readonly type = SET_EXPENSE_TYPES;

  constructor(public payload: Array<ExpenseType>) {
  }
}

export class AddExpenseType implements Action {
  readonly type = ADD_EXPENSE_TYPE;

  constructor(public payload: ExpenseType) {
  }
}

export type ExpenseTypeActions = SetExpenseTypes | AddExpenseType;
