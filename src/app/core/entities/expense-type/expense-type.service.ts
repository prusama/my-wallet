import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";

import * as fromExpenseTypes from './expense-type.reducer';
import * as ExpenseActions from './expense-type.actions';
import {ExpenseType} from "./expense-type";
import {ExpenseTypeRepositoryService} from "./expense-type-repository.service";
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ExpenseTypeService {
  private subs: Array<Subscription> = [];

  constructor(
    private $store: Store<fromExpenseTypes.State>,
    private $expenseTypesRepo: ExpenseTypeRepositoryService
  ) {
  }

  public addExpenseType(expenseType: ExpenseType): void {
    this.$expenseTypesRepo.insertExpenseType(expenseType);
    this.$store.dispatch(new ExpenseActions.AddExpenseType(expenseType));
  }

  public getAllExpenseTypes(): void {
    this.subs.push(
      this.$expenseTypesRepo.getExpenseTypes().subscribe((expenseTypes: Array<ExpenseType>) => {
        this.$store.dispatch(new ExpenseActions.SetExpenseTypes(expenseTypes));
      })
    );
  }

  public cancelSubscriptions(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
