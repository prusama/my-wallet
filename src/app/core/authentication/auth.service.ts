import { Injectable } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {AuthData} from "./auth-data";
import {Subject} from "rxjs";
import {Router} from "@angular/router";
import {TransactionRepositoryService} from "../entities/transaction/transaction-repository.service";
import {LoadingService} from "../components/loading/loading.service";
import {Store} from "@ngrx/store";

import * as fromRoot from '../../app.reducer';
import * as Loading from '../../core/components/loading/loading.actions';
import * as Auth from '../../core/authentication/auth.actions';
import {UserDetail} from "../entities/user/user-detail";
import {UserDetailRepositoryService} from "../entities/user/user-detail-repository.service";
import {ExpenseTypeService} from "../entities/expense-type/expense-type.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private $auth: AngularFireAuth,
    private $router: Router,
    private $transactionRepo: TransactionRepositoryService,
    private $userDetailRepo: UserDetailRepositoryService,
    private $expenseTypesService: ExpenseTypeService,
    private $loadingService: LoadingService,
    private $store: Store<fromRoot.State>
  ) { }

  initAuthListener() {
    this.$auth.authState.subscribe((authUser) => {
      if (authUser) {
        this.$store.dispatch(new Auth.SetAuthenticated());
        this.$userDetailRepo.getUserByAuthUserUID(authUser.uid);
        this.$router.navigate(['/wallet']);
      } else {
        this.$transactionRepo.cancelSubscriptions();
        this.$userDetailRepo.cancelSubscriptions();
        this.$expenseTypesService.cancelSubscriptions();
        this.$store.dispatch(new Auth.SetUnAuthenticated());
        this.$router.navigate(['/sign/in']);
      }
    });
  }

  public register(authData: AuthData, userDetail: UserDetail) {
    this.$store.dispatch(new Loading.StartLoading());
    this.$auth.createUserWithEmailAndPassword(authData.email, authData.password).then((result) => {
      console.log(result);

      userDetail.authUserUID = result.user.uid;
      this.$userDetailRepo.addUserDetail(userDetail);

      this.$store.dispatch(new Loading.StopLoading());
    }).catch((error) => {
      console.log(error);
      this.$store.dispatch(new Loading.StopLoading());
    });
  }

  public login(authData: AuthData) {
    this.$store.dispatch(new Loading.StartLoading());
    this.$auth.signInWithEmailAndPassword(authData.email, authData.password).then((result) => {
      console.log(result);
      this.$store.dispatch(new Loading.StopLoading());
    }).catch((error) => {
      console.log(error);
      this.$store.dispatch(new Loading.StopLoading());
    });
  }

  public logout() {
    this.$auth.signOut();
  }
}
