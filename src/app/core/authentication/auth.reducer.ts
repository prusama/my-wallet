import {AuthActions, LOAD_USER_DETAIL, SET_AUTHENTICATED, SET_UNAUTHENTICATED} from "./auth.actions";
import {UserDetail} from "../entities/user/user-detail";
import {createFeatureSelector, createSelector} from "@ngrx/store";

export interface State {
  isAuthenticated: boolean;
  userDetail: UserDetail;
}

const initialState: State = {
  isAuthenticated: false,
  userDetail: null
};

export function authReducer(state: State = initialState, action: AuthActions) {
  switch (action.type) {
    case SET_AUTHENTICATED:
      return {
        isAuthenticated: true,
        userDetail: null
      };
    case LOAD_USER_DETAIL:
      return {
        isAuthenticated: true,
        userDetail: action.payload
      };
    case SET_UNAUTHENTICATED:
      return {
        isAuthenticated: false,
        userDetail: null
      };
    default:
      return state;
  }
}

// Helper method
export const getIsAuth = (state: State) => state.isAuthenticated;
export const getUserDetail = (state: State) => state.userDetail;
