import {Action} from "@ngrx/store";
import {UserDetail} from "../entities/user/user-detail";

export const SET_AUTHENTICATED = '[Auth] Set Authenticated';
export const LOAD_USER_DETAIL = '[Auth] Load User Detail';
export const SET_UNAUTHENTICATED = '[Auth] Set Unauthenticated';

export class SetAuthenticated implements Action {
  readonly type = SET_AUTHENTICATED;
}

export class LoadUserDetail implements Action {
  readonly type = LOAD_USER_DETAIL;

  constructor(public payload: UserDetail) {}
}

export class SetUnAuthenticated implements Action {
  readonly type = SET_UNAUTHENTICATED;
}

export type AuthActions = SetAuthenticated | SetUnAuthenticated | LoadUserDetail;
