import {NgModule} from "@angular/core";
import {LayoutComponent} from "./layout.component";
import {UserTileComponent} from "./user-tile/user-tile.component";
import {NavigationComponent} from "./navigation/navigation.component";
import {SharedModule} from "../../../shared/shared-modules/shared.module";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    LayoutComponent,
    UserTileComponent,
    NavigationComponent
  ],
  exports: [
    LayoutComponent
  ],
  imports: [
    SharedModule,
    RouterModule
  ]
})
export class LayoutModule {

}
