import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {UserDetail} from "../../../entities/user/user-detail";

import * as fromRoot from "../../../../app.reducer";

@Component({
  selector: 'mw-user-tile',
  templateUrl: './user-tile.component.html',
  styleUrls: ['./user-tile.component.scss'],
})
export class UserTileComponent implements OnInit {
  public userDetail: UserDetail;

  constructor(
    private $store: Store<fromRoot.State>
  ) { }

  ngOnInit(): void {
    this.$store.select(fromRoot.getUserDetail).subscribe((userDetail: UserDetail) => {
      this.userDetail = userDetail;
    });
  }

}
