import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../authentication/auth.service";

import * as fromRoot from '../../../app.reducer';
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {MatDrawer} from "@angular/material/sidenav";

@Component({
  selector: 'mw-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {
  @ViewChild('drawer') drawer: MatDrawer;
  public isAuth$: Observable<boolean>;

  constructor(
    private $authService: AuthService,
    private $store: Store<fromRoot.State>
  ) { }

  ngOnInit(): void {
    this.isAuth$ = this.$store.select(fromRoot.getIsAuth);
  }

  ngAfterViewInit(): void {
    this.$store.select(fromRoot.getIsAuth).subscribe((isAuth: boolean) => {
      this.drawer.opened = isAuth;
    });
  }

  public logout(): void {
    this.$authService.logout();
  }
}
