import { Component, OnInit } from '@angular/core';
import {LoadingService} from "./loading.service";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {map} from 'rxjs/operators';

import * as fromRoot from '../../../app.reducer';

@Component({
  selector: 'mw-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  public isLoading$: Observable<boolean>;

  constructor(
    private $loadingService: LoadingService,
    private $store: Store<fromRoot.State>
  ) { }

  ngOnInit(): void {
    this.isLoading$ = this.$store.select(fromRoot.getIsLoading);
  }

}
