import {LoadingActions, START_LOADING, STOP_LOADING} from "./loading.actions";

export interface State {
  isLoading: boolean;
}

const initialState: State = {
  isLoading: false
};

export function loadingReducer(state: State = initialState, action: LoadingActions) {
  switch (action.type) {
    case START_LOADING:
      return {
        isLoading: true
      }
    case STOP_LOADING:
      return {
        isLoading: false
      }
    default: {
      return state;
    }
  }
}

export const getIsLoading = (state: State) => state.isLoading;
