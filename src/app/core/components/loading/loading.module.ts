import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading.component';
import {MaterialDesignModule} from "../../../shared/material-design/material-design.module";



@NgModule({
  declarations: [LoadingComponent],
  exports: [
    LoadingComponent
  ],
  imports: [
    CommonModule,
    MaterialDesignModule
  ]
})
export class LoadingModule { }
