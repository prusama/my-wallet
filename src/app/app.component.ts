import {Component, OnInit} from '@angular/core';
import {AuthService} from "./core/authentication/auth.service";

@Component({
  selector: 'mw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private $authService: AuthService) {
  }

  ngOnInit() {
    this.$authService.initAuthListener();
  }
}
