import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LayoutModule} from "./core/components/layout/layout.module";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {MaterialDesignModule} from "./shared/material-design/material-design.module";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {LoadingModule} from "./core/components/loading/loading.module";
import {StoreModule} from "@ngrx/store";
import {reducers} from "./app.reducer";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    LoadingModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialDesignModule,
    LayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    StoreModule.forRoot(reducers)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
