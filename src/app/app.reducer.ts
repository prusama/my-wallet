import {ActionReducerMap, createFeatureSelector, createSelector} from "@ngrx/store";

import * as fromLoading from './core/components/loading/loading.reducer';
import * as fromAuth from './core/authentication/auth.reducer';

export interface State {
  loading: fromLoading.State;
  auth: fromAuth.State;
}

export const reducers: ActionReducerMap<State> = {
  loading: fromLoading.loadingReducer,
  auth: fromAuth.authReducer
}

export const getLoadingState = createFeatureSelector<fromLoading.State>('loading');
export const getIsLoading = createSelector(getLoadingState, fromLoading.getIsLoading);

export const getAuthState = createFeatureSelector<fromAuth.State>('auth');
export const getIsAuth = createSelector(getAuthState, fromAuth.getIsAuth);
export const getUserDetail = createSelector(getAuthState, fromAuth.getUserDetail);
