import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./core/authentication/auth.guard";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'wallet',
    pathMatch: 'full'
  },
  {
    path: 'sign',
    loadChildren: () => import('./modules/sign/sign.module').then(m => m.SignModule)
  },
  {
    path: 'wallet',
    loadChildren: () => import('./modules/wallet/wallet.module').then(m => m.WalletModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'expense-types',
    loadChildren: () => import('./modules/expense-types/expense-types.module').then(m => m.ExpenseTypesModule),
    canLoad: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
