import { NgModule } from '@angular/core';
import { ExpenseTypesOverviewComponent } from './pages/expense-types-overview/expense-types-overview.component';
import {ExpenseTypesListComponent} from "./components/expense-types-list/expense-types-list.component";
import {SharedModule} from "../../shared/shared-modules/shared.module";
import {ExpenseTypesRoutingModule} from "./expense-types-routing.module";
import { ExpenseTypesEditorComponent } from './components/expense-types-editor/expense-types-editor.component';
import { AddExpenseTypeComponent } from './pages/add-expense-type/add-expense-type.component';
import {StoreModule} from "@ngrx/store";
import {expenseTypeReducer} from "../../core/entities/expense-type/expense-type.reducer";



@NgModule({
  declarations: [
    ExpenseTypesListComponent,
    ExpenseTypesOverviewComponent,
    ExpenseTypesEditorComponent,
    AddExpenseTypeComponent,
  ],
  imports: [
    SharedModule,
    ExpenseTypesRoutingModule,
    StoreModule.forFeature('expenseType', expenseTypeReducer)
  ]
})
export class ExpenseTypesModule { }
