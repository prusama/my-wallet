import {RouterModule, Routes} from "@angular/router";
import {ExpenseTypesOverviewComponent} from "./pages/expense-types-overview/expense-types-overview.component";
import {NgModule} from "@angular/core";
import {AddExpenseTypeComponent} from "./pages/add-expense-type/add-expense-type.component";

const routes: Routes = [
  {path: '', component: ExpenseTypesOverviewComponent},
  {path: 'add-expense-type', component: AddExpenseTypeComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseTypesRoutingModule { }
