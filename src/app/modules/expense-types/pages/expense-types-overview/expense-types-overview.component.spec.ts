import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseTypesOverviewComponent } from './expense-types-overview.component';

describe('ExpenseTypesOverviewComponent', () => {
  let component: ExpenseTypesOverviewComponent;
  let fixture: ComponentFixture<ExpenseTypesOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpenseTypesOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseTypesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
