import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mw-expense-types-overview',
  templateUrl: './expense-types-overview.component.html',
  styleUrls: ['./expense-types-overview.component.scss']
})
export class ExpenseTypesOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
