import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {ExpenseType} from "../../../../core/entities/expense-type/expense-type";
import {ExpenseTypeService} from "../../../../core/entities/expense-type/expense-type.service";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Store} from "@ngrx/store";

import * as fromExpenseTypes from '../../../../core/entities/expense-type/expense-type.reducer';

@Component({
  selector: 'mw-expense-types-list',
  templateUrl: './expense-types-list.component.html',
  styleUrls: ['./expense-types-list.component.scss']
})
export class ExpenseTypesListComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSort) sort?: MatSort;
  @ViewChild(MatPaginator) paginator?: MatPaginator;

  public displayedColumns: Array<String> = ['name', 'color'];
  public dataSource = new MatTableDataSource<ExpenseType>();

  constructor(
    private $expenseTypeService: ExpenseTypeService,
    private $store: Store<fromExpenseTypes.State>
  ) { }

  ngOnInit(): void {
    this.$store.select(fromExpenseTypes.getExpenseTypes).subscribe((expenseTypes: Array<ExpenseType>) => {
      this.dataSource.data = expenseTypes;
    });
    this.$expenseTypeService.getAllExpenseTypes();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
