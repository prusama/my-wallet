import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseTypesEditorComponent } from './expense-types-editor.component';

describe('ExpenseTypesEditorComponent', () => {
  let component: ExpenseTypesEditorComponent;
  let fixture: ComponentFixture<ExpenseTypesEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpenseTypesEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseTypesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
