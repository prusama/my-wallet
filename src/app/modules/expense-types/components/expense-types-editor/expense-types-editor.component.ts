import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ExpenseType} from "../../../../core/entities/expense-type/expense-type";
import {ExpenseTypeService} from "../../../../core/entities/expense-type/expense-type.service";
import {Router} from "@angular/router";

@Component({
  selector: 'mw-expense-types-editor',
  templateUrl: './expense-types-editor.component.html',
  styleUrls: ['./expense-types-editor.component.scss']
})
export class ExpenseTypesEditorComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private $formBuilder: FormBuilder,
    private $expenseTypeService: ExpenseTypeService,
    private $router: Router
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  public submitForm(): void {
    const values = this.form.value;

    const expenseType: ExpenseType = {
      name: values.name,
      color: values.color
    };

    this.$expenseTypeService.addExpenseType(expenseType);
    this.$router.navigate(['/expense-types']);
  }

  private buildForm(): void {
    this.form = this.$formBuilder.group({
      name: ['', [Validators.required]],
      color: ['', [Validators.required]]
    });
  }
}
