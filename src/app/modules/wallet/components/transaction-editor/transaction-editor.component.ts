import { Component, OnInit } from '@angular/core';
import {TransactionRepositoryService} from "../../../../core/entities/transaction/transaction-repository.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Transaction} from "../../../../core/entities/transaction/transaction";

import * as fromTransaction from '../../../../core/entities/transaction/transaction.reducer';
import * as TransactionAction from '../../../../core/entities/transaction/transaction.actions';
import {Store} from "@ngrx/store";

@Component({
  selector: 'mw-transaction-editor',
  templateUrl: './transaction-editor.component.html',
  styleUrls: ['./transaction-editor.component.scss']
})
export class TransactionEditorComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private $transactionRepo: TransactionRepositoryService,
    private $formBuilder: FormBuilder,
    private $store: Store<fromTransaction.State>
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  public submitForm(): void {
    const values = this.form.value;

    // TODO: If new transaction
    const transaction: Transaction = {
      name: values.name,
      amount: values.amount,
      date: new Date()
    };

    this.$transactionRepo.addTransaction(transaction);

    /*this.$transactionRepo.addTransaction({
      name: values.name,
      amount: values.amount,
      date: new Date()
    });*/
  }

  private buildForm(): void {
    this.form = this.$formBuilder.group({
      name: ['', [Validators.required]],
      amount: ['', [Validators.required]],
    });
  }

}
