import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {Transaction} from "../../../../core/entities/transaction/transaction";
import {Subscription} from "rxjs";
import {TransactionRepositoryService} from "../../../../core/entities/transaction/transaction-repository.service";
import {Store} from "@ngrx/store";

import * as fromTransaction from '../../../../core/entities/transaction/transaction.reducer';

@Component({
  selector: 'mw-transaction-table',
  templateUrl: './transaction-table.component.html',
  styleUrls: ['./transaction-table.component.scss']
})
export class TransactionTableComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSort) sort?: MatSort;
  @ViewChild(MatPaginator) paginator?: MatPaginator;

  public displayedColumns = ['name', 'amount'];
  public dataSource = new MatTableDataSource<Transaction>();

  constructor(
    private $transactionRepository: TransactionRepositoryService,
    private $store: Store<fromTransaction.State>
  ) {
  }

  ngOnInit(): void {
    this.$store.select(fromTransaction.getTransactions).subscribe((transactions) => {
      this.dataSource.data = transactions;
    });
    this.$transactionRepository.getAllTransactions();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
