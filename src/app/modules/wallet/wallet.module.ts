import {NgModule} from '@angular/core';
import {WalletRoutingModule} from './wallet-routing.module';
import {OverviewComponent} from './pages/overview/overview.component';
import {TransactionTableComponent} from './components/transaction-table/transaction-table.component';
import {AddTransactionComponent} from './pages/add-transaction/add-transaction.component';
import {TransactionEditorComponent} from './components/transaction-editor/transaction-editor.component';
import {SharedModule} from "../../shared/shared-modules/shared.module";
import {StoreModule} from "@ngrx/store";
import {transactionReducer} from "../../core/entities/transaction/transaction.reducer";

@NgModule({
  declarations: [
    OverviewComponent,
    TransactionTableComponent,
    AddTransactionComponent,
    TransactionEditorComponent,
  ],
  imports: [
    SharedModule,
    WalletRoutingModule,
    StoreModule.forFeature('transaction', transactionReducer)
  ]
})
export class WalletModule {
}
