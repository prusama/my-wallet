import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {OverviewComponent} from "./pages/overview/overview.component";
import {AddTransactionComponent} from "./pages/add-transaction/add-transaction.component";

const routes: Routes = [
  {path: '', component: OverviewComponent},
  {path: 'add-transaction', component: AddTransactionComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WalletRoutingModule { }
