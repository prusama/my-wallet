import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../../core/authentication/auth.service";
import {AuthData} from "../../../../core/authentication/auth-data";
import {UserDetail} from "../../../../core/entities/user/user-detail";

@Component({
  selector: 'mw-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss']
})
export class SignUpFormComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private $formBuilder: FormBuilder,
    private $authService: AuthService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  public submitForm(): void {
    const values = this.form.value;

    const authData: AuthData = {
      email: values.email,
      password: values.password
    };

    const userDetail: UserDetail = {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      dateOfBirth: values.dateOfBirth
    }

    this.$authService.register(authData, userDetail);
  }

  private buildForm(): void {
    this.form = this.$formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      passwordAgain: ['', [Validators.required]],
      dateOfBirth: ['', [Validators.required]],
      agreement: ['', [Validators.required]],
    });
  }

}
