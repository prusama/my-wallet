import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../../core/authentication/auth.service";

@Component({
  selector: 'mw-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private $formBuilder: FormBuilder,
    private $authService: AuthService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  public submitForm(): void {
    const values = this.form.value;

    this.$authService.login({
      email: values.email,
      password: values.password
    });
  }

  private buildForm(): void {
    this.form = this.$formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

}
