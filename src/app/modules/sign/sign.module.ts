import { NgModule } from '@angular/core';

import { SignRoutingModule } from './sign-routing.module';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared-modules/shared.module";
import { SignUpFormComponent } from './components/sign-up-form/sign-up-form.component';


@NgModule({
  declarations: [SignUpComponent, SignInComponent, LoginFormComponent, SignUpFormComponent],
  imports: [
    SharedModule,
    SignRoutingModule,
  ]
})
export class SignModule { }
